package es.lagrupeta.sp.infrastructure.api;

import es.lagrupeta.sp.application.routes.CreateRouteUseCase;
import es.lagrupeta.sp.application.routes.GetRouteByIdUseCase;
import es.lagrupeta.sp.application.routes.GetRoutesUseCase;
import es.lagrupeta.sp.application.routes.request.CreateRouteRequest;
import es.lagrupeta.sp.domain.routes.Filter;
import es.lagrupeta.sp.domain.routes.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/routes")
public class RoutesController {

    @Autowired
    private GetRoutesUseCase getRoutesUseCase;

    @Autowired
    private GetRouteByIdUseCase getRouteByIdUseCase;

    @Autowired
    private CreateRouteUseCase createRouteUseCase;

    @GetMapping
    List<Route> find(@RequestParam String name) {
        return getRoutesUseCase.find(new Filter(
                name
        ));
    }

    @GetMapping("/{id}")
    Route findById(@PathVariable String id) {
        return getRouteByIdUseCase.findById(UUID.fromString(id));
    }

    @PostMapping
    Route create(@RequestBody CreateRouteRequest request) {
        return createRouteUseCase.createRoute(request);
    }
}
