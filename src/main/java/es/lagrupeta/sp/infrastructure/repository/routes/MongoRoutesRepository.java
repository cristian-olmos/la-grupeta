package es.lagrupeta.sp.infrastructure.repository.routes;

import es.lagrupeta.sp.domain.routes.Route;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MongoRoutesRepository extends MongoRepository<Route, UUID> {
    List<Route> findByName(String name);
}
