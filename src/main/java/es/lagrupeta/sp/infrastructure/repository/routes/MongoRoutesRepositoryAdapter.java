package es.lagrupeta.sp.infrastructure.repository.routes;

import es.lagrupeta.sp.domain.routes.Filter;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.domain.routes.RoutesRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class MongoRoutesRepositoryAdapter implements RoutesRepository {

    MongoRoutesRepository mongoRoutesRepository;

    public MongoRoutesRepositoryAdapter(MongoRoutesRepository mongoRoutesRepository) {
        this.mongoRoutesRepository = mongoRoutesRepository;
    }

    @Override
    public List<Route> find(Filter filter) {
        return mongoRoutesRepository.findByName(filter.getName());
    }

    @Override
    public Optional<Route> findById(UUID uuid) {
        return mongoRoutesRepository.findById(uuid);
    }

    @Override
    public Route save(Route route) {
        return mongoRoutesRepository.save(route);
    }
}
