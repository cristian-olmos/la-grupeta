package es.lagrupeta.sp.infrastructure.boot;

import es.lagrupeta.sp.infrastructure.repository.routes.MongoRoutesRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan("es.lagrupeta.sp")
@EnableMongoRepositories(basePackageClasses = MongoRoutesRepository.class)
public class SpApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpApplication.class, args);
    }

}
