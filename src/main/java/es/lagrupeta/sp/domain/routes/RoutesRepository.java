package es.lagrupeta.sp.domain.routes;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoutesRepository {

    List<Route> find(Filter filter);

    Optional<Route> findById(UUID uuid);

    Route save(Route route);
}
