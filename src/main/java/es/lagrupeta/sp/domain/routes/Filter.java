package es.lagrupeta.sp.domain.routes;

public class Filter {
    private String name;

    public Filter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "name='" + name + '\'' +
                '}';
    }
}
