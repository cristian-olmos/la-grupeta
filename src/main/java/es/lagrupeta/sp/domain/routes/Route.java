package es.lagrupeta.sp.domain.routes;

import java.util.UUID;

public class Route {
    private final UUID id;
    private final String name;

    private Route(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Route create(String name) {
        return new Route(
                UUID.randomUUID(),
                name
        );
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
