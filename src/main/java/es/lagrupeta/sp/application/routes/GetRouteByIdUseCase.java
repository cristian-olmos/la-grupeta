package es.lagrupeta.sp.application.routes;

import es.lagrupeta.sp.domain.exception.NotFoundException;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.domain.routes.RoutesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GetRouteByIdUseCase {

    private static final Logger log = LoggerFactory.getLogger(GetRouteByIdUseCase.class);

    private final RoutesRepository routesRepository;

    public GetRouteByIdUseCase(RoutesRepository routesRepository) {
        this.routesRepository = routesRepository;
    }

    public Route findById(UUID uuid) {
        log.info("Checking route by id {}", uuid);
        return routesRepository.findById(uuid).orElseThrow(() -> new NotFoundException("Route " + uuid + " not found"));
    }

}
