package es.lagrupeta.sp.application.routes.request;

public class CreateRouteRequest {
    private final String name;
    private final Double lowerLat;
    private final Double leftLon;
    private final Double upperLat;
    private final Double rightLon;

    public CreateRouteRequest(String name, Double lowerLat, Double leftLon, Double upperLat, Double rightLon) {
        this.name = name;
        this.lowerLat = lowerLat;
        this.leftLon = leftLon;
        this.upperLat = upperLat;
        this.rightLon = rightLon;
    }

    public String getName() {
        return name;
    }

    public Double getLowerLat() {
        return lowerLat;
    }

    public Double getLeftLon() {
        return leftLon;
    }

    public Double getUpperLat() {
        return upperLat;
    }

    public Double getRightLon() {
        return rightLon;
    }
}
