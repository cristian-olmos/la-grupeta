package es.lagrupeta.sp.application.routes;

import es.lagrupeta.sp.application.routes.request.CreateRouteRequest;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.domain.routes.RoutesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreateRouteUseCase {

    private static final Logger log = LoggerFactory.getLogger(CreateRouteUseCase.class);

    private final RoutesRepository routesRepository;

    public CreateRouteUseCase(RoutesRepository routesRepository) {
        this.routesRepository = routesRepository;
    }

    public Route createRoute(CreateRouteRequest request) {
        log.info("Creating request  {}", request);
        Route route = Route.create(
                request.getName()
        );
        return routesRepository.save(route);
    }

}
