package es.lagrupeta.sp.application.routes;

import es.lagrupeta.sp.domain.routes.Filter;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.domain.routes.RoutesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetRoutesUseCase {

    private static final Logger log = LoggerFactory.getLogger(GetRoutesUseCase.class);

    private final RoutesRepository routesRepository;

    public GetRoutesUseCase(RoutesRepository routesRepository) {
        this.routesRepository = routesRepository;
    }

    public List<Route> find(Filter filter) {
        log.info("Checking routes for filter {}", filter);
        return routesRepository.find(filter);
    }

}
