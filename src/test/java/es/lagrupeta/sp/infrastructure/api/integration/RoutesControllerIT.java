package es.lagrupeta.sp.infrastructure.api.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.infrastructure.boot.SpApplication;
import es.lagrupeta.sp.infrastructure.repository.routes.MongoRoutesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SpApplication.class)
@AutoConfigureMockMvc
class RoutesControllerIT {

    private static final String MENDRUGO = "Mendrugo";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MongoRoutesRepository routesRepository;

    @BeforeEach
    void setUp() {
        routesRepository.deleteAll();
    }

    @Test
    void given_route_when_find_route_by_id_then_return_route() throws Exception {

        Route save = routesRepository.save(Route.create(MENDRUGO));

        MvcResult mvcResult = mockMvc.perform(get("/api/v1/routes/{id}", save.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
        Route response = objectMapper.readValue(contentAsString, Route.class);
        assertThat(response.getId()).isEqualTo(save.getId());

    }

}