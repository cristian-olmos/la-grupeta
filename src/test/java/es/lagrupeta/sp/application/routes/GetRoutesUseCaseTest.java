package es.lagrupeta.sp.application.routes;

import es.lagrupeta.sp.domain.routes.Filter;
import es.lagrupeta.sp.domain.routes.Route;
import es.lagrupeta.sp.domain.routes.RoutesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class GetRoutesUseCaseTest {

    public static final String ROUTE_NAME = "Mendrugo";
    private RoutesRepository routesRepository;

    private GetRoutesUseCase getRoutesUseCase;

    @BeforeEach
    public void init() {
        routesRepository = Mockito.mock(RoutesRepository.class);
        getRoutesUseCase = new GetRoutesUseCase(routesRepository);
    }

    @Test
    public void given_valid_filter_when_find_routes_then_return_all_routes() {
        Filter filter = new Filter("Mendrugo");
        when(routesRepository.find(filter)).thenReturn(getRoutes());

        List<Route> routes = getRoutesUseCase.find(filter);

        assertThat(routes.get(0).getName()).isEqualTo(ROUTE_NAME);

    }

    private List<Route> getRoutes() {
        return List.of(
                Route.create(ROUTE_NAME)
        );
    }

}